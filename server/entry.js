var Promise = require('bluebird');
var debug = require('debug-logger');
var express = require('express');
var fs = require('fs');
var _ = require('lodash');
var config = require('./config');
var StreamZip = require('node-stream-zip');
var app = express();
var logger = debug('content-server');

var library = config.libraryRoot;

function ensureExtractDir() {
    var dir = __dirname + '/extract/';
    try {
        fs.lstatSync(dir);
    }
    catch(e) {
        fs.mkdir(dir);
    }
    return dir;
}
function findArchives(bookId) {
    return new Promise(function(resolve, reject) {
        var bookIndex = parseInt(bookId);
        if (isNaN(bookIndex))
            return reject(new Error('invalid book id'));

        fs.readdir(library, function(err, files) {
            var result = [];

            _.forEach(files, function(file) {
                var regexp = /^fb2-([0-9]*)-([0-9]*).*zip/g;
                var match = regexp.exec(file);

                if (match && match[1] && match[2]) {
                    var min = parseInt(match[1]);
                    var max = parseInt(match[2]);
                    if (!isNaN(min) && !isNaN(max)) {
                        if ((bookIndex >= min) && (bookIndex <= max))
                            result.push(file);
                    }
                }
            });

            return resolve(result);
        });
    });
}
function extractBook(bookId, archives) {
    return new Promise(function(resolve, reject) {
        Promise.map(archives, function (archive) {
                return new Promise(function (resolve, reject) {
                    var zip = new StreamZip({
                        file: library + archive,
                        storeEntries: true
                    });

                    var waitExtraction = false;

                    zip.on('entry', function (entry) {
                        if (_.startsWith(entry.name, bookId)) {
                            waitExtraction = true;
                            var extractDir = ensureExtractDir();
                            zip.extract(entry.name, extractDir, function(err) {
                                if (err)
                                    return reject(err);
                                return resolve(extractDir + entry.name);
                            });
                        }
                    });
                    zip.on('ready', function () {
                        if (!waitExtraction)
                            return resolve();
                    });
                });
            })
            .then(function (extractList) {
                _.forEach(extractList, function(book) {
                    if (book)
                        return resolve(book);
                });
                return reject(new Error('book not founded'));
            })
    });
}



app.get('/', function (req, res) {
    res.send('Content server is running');
});

app.get('/book', function (req, res) {
    var bookid = req.query.id;

    if (bookid) {
        findArchives(bookid)
            .then(function (entries) {
                return extractBook(bookid, entries);
            })
            .then(function (book) {
                res.download(book);
            })
            .catch(function (err) {
                logger.error(err);
                res.status(404);
                res.send('Book not found');
            });
    }


});

var server = app.listen(40404, function () {
    logger.info('start listening on port %s', server.address().port);
});


