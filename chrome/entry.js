function removeOverlay() {
    $('.lsp-overlay').remove();
    $('style').remove();
}
function patchHrefs() {
    var regex = new RegExp("^.*скачать.*");

    $('a')
        .filter(function () {
            return regex.test($(this).text());
        })
        .each(function() {
            var parts = this.href.replace('http://', '').split('/');
            for (var index = parts.length - 1; index >= 0; index--) {
                if ((parts[index] === 'download') && (index > 0)) {
                    var bookIndex = parts[index - 1];
                    this.href = 'http://localhost:40404/book?id=' + bookIndex;
                }
            }
        });
}

removeOverlay();
patchHrefs();